<?php

// autoload_static.php @generated by Composer

namespace Composer\Autoload;

class ComposerStaticInit945698fdd9b6e673d17d41d666b37cbf
{
    public static $prefixLengthsPsr4 = array (
        'A' => 
        array (
            'App\\' => 4,
        ),
    );

    public static $prefixDirsPsr4 = array (
        'App\\' => 
        array (
            0 => __DIR__ . '/../..' . '/src/BITM/SEIP143471',
        ),
    );

    public static function getInitializer(ClassLoader $loader)
    {
        return \Closure::bind(function () use ($loader) {
            $loader->prefixLengthsPsr4 = ComposerStaticInit945698fdd9b6e673d17d41d666b37cbf::$prefixLengthsPsr4;
            $loader->prefixDirsPsr4 = ComposerStaticInit945698fdd9b6e673d17d41d666b37cbf::$prefixDirsPsr4;

        }, null, ClassLoader::class);
    }
}
